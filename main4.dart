import "package:flutter/material.dart";
import 'package:mtn_app/app_page/Home_Sreen.dart';
import 'app_page/Login_Screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(primarySwatch: Colors.yellow), home: LoginPage());
  }

  @override
  _HomepageScreenState createState() => _HomepageScreenState();
}

class _HomepageScreenState extends State<HomepageScreen> {
  Future<FirebaseApp> _initializerFirebase() async {
    FirebaseApp firebase = await Firebase.initializer();
    return firebase;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: FutureBuilder(
            future: _initializerFirebase(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                return LoginPage();
              }
              return const Center(
                child: CircularProgressIndicator(),
              );
            }));
  }
}
