import 'package:flutter/cupertino_1.dart';
import 'package:flutter/material.dart';
import 'package:mtn_app/app_page/Formpage.dart';
import 'package:mtn_app/app_page/Login_Screen.dart';

class HomepageScreen extends StatefulWidget {
  const HomepageScreen({Key? key}) : super(key: key);

  @override
  _HomepageScreenState createState() => _HomepageScreenState();
}

class _HomepageScreenState extends State<HomepageScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('GALLARY'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(10),
          child: ListView(children: <Widget>[
            Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.all(10),
                child: const Text('WELCOM TO MTN APP ACADEMY',
                    style: TextStyle(
                        color: Colors.black54,
                        fontWeight: FontWeight.w500,
                        fontSize: 30))),
            Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.all(10),
                child: const Text(
                    'The 2022 App Academy is once again SETA accredited.',
                    style: TextStyle(
                        color: Colors.black54,
                        fontWeight: FontWeight.w500,
                        fontSize: 20))),
            Container(
                height: 129,
                child: const Image(
                  image: AssetImage('images/logo_MTN.png'),
                )),
            Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.all(10),
                child: const Text('DRIVE DIGITAL SOLUTION FORWARD.',
                    style: TextStyle(
                        color: Color.fromRGBO(211, 192, 18, 0.82),
                        fontWeight: FontWeight.w500,
                        fontSize: 20))),
            Container(
                height: 45,
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: RaisedButton(
                  textColor: Colors.black,
                  color: Colors.yellow,
                  child: const Text('LOG-OUT'),
                  onPressed: () {
                    Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (context) => LoginPage()));
                  },
                )),
            Container(
                height: 45,
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: RaisedButton(
                  textColor: Colors.yellow,
                  color: Colors.black,
                  child: const Text('Click App Form'),
                  onPressed: () {
                    Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (context) => Formpage()));
                  },
                )),
          ]),
        ));
  }
}
