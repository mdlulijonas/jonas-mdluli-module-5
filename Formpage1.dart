import 'package:flutter/cupertino_1.dart';
import 'package:flutter/material.dart';
import 'package:mtn_app/app_page/Home_Sreen.dart';
import 'package:mtn_app/app_page/Login_Screen.dart';

class Formpage extends StatefulWidget {
  const Formpage({Key? key}) : super(key: key);

  @override
  _FormpageState createState() => _FormpageState();
}

class _FormpageState extends State<Formpage> {
  TextEditingController IDNumberController = TextEditingController();
  TextEditingController RaceController = TextEditingController();
  TextEditingController CountryController = TextEditingController();
  TextEditingController HighestGradeController = TextEditingController();
  TextEditingController GenderController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('FORM Page'),
      ),
      body: Padding(
          padding: const EdgeInsets.all(10),
          child: ListView(children: <Widget>[
            Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.all(10),
                child: const Text('MTN APP ACADEMY',
                    style: TextStyle(
                        color: Colors.black54,
                        fontWeight: FontWeight.w500,
                        fontSize: 30))),
            Container(
                padding: const EdgeInsets.all(10),
                child: TextField(
                    controller: IDNumberController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'IDNumber',
                    ))),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                controller: RaceController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'RACE',
                ),
              ),
            ),
            Container(
                padding: const EdgeInsets.all(10),
                child: TextField(
                    controller: CountryController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Country',
                    ))),
            Container(
                padding: const EdgeInsets.all(10),
                child: TextField(
                    controller: HighestGradeController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Highest Grade',
                    ))),
            Container(
                padding: const EdgeInsets.all(10),
                child: TextField(
                    controller: GenderController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Gender',
                    ))),
            Container(
                height: 45,
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: RaisedButton(
                  textColor: Colors.black,
                  color: Colors.yellow,
                  child: const Text('Apply'),
                  onPressed: () {
                    print(IDNumberController.text);
                    print(RaceController.text);
                    print(CountryController.text);
                    print(HighestGradeController.text);
                    print(GenderController.text);

                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => const HomepageScreen()));
                  },
                )),
            Container(
                height: 45,
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: RaisedButton(
                    textColor: Colors.white,
                    color: Colors.black,
                    child: const Text('Log-Out'),
                    onPressed: () {
                      Navigator.of(context).pushReplacement(
                          MaterialPageRoute(builder: (context) => LoginPage()));
                    }))
          ])),
    );
  }
}
